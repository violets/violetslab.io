# 🔍 Search with Bing
<div class="srch">
      <form action="https://cn.bing.com/search" target="_self" method="get">
        <input
          type="text"
          id="search-input"
          name="q"
          placeholder="console.log('佬，今天搜索点什么呢? 🫠') "
          autofocus
        />
      </form>
</div>

---

# 📦 Git

- [Github](https://github.com)
- [Gitlab](https://gitlab.com)

---

# 📚 Tutorials

- [Arch Wiki](https://wiki.archlinuxcn.org)
- [Arch Linux 简明指南](https://arch.icekylin.online/)

---

# 🔧 Tools

- [图片取色](https://www.jyshare.com/front-end/6214/#25bbd9)
- [USTC 测速](https://test.ustc.edu.cn)
- [生成 ASCII 艺术字](https://www.lddgo.net/string/text-to-ascii-art)
- [密码字典](https://weakpass.com/)
- [图片搜索](https://saucenao.com)
- [模糊图片](https://imgonline.tools/zh/blur)

---

# 🪞 Mirrors (Linux)

- [Tsinghua](https://mirrors.tuna.tsinghua.edu.cn)
- [USTC](https://mirrors.ustc.edu.cn)
- [ArchlinuxCN](https://repo.archlinuxcn.org)

---

# 🎵 Amusement

- [Bilibili](https://bilibili.com)
- [Apple Music](https://music.apple.com/cn)
- [My Free MP3](https://tools.liumingye.cn/music)
